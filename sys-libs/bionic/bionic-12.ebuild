# Copyright 2016-2021 Ataraxia GNU/Linux
# Distributed under the terms of the Affero GNU General Public License v3

EAPI=8

inherit toolchain-funcs

MY_PV="12b5"

DESCRIPTION="bionic is Android's C library, math library, and dynamic linker"
HOMEPAGE="https://android.googlesource.com/platform/bionic/+/refs/heads/master/README.md"
SRC_URI="https://ataraxialinux.org/images/distfiles/${PN}-${MY_PV}.tar.gz"
S="${WORKDIR}"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~arm64 ~arm ~x86"

PROVIDE="
	elibc_bionic? ( virtual/libc )
	elibc_bionic? ( virtual/os-headers )
"
BDEPEND="
	sys-devel/llvm
	dev-lang/python:3
	dev-lang/python:2
"

bionic_arch() {
	case "$1" in
		amd64) echo "x86_64" ;;
		arm64) echo "arm64" ;;
		arm) echo "arm" ;;
		x86) echo "x86" ;;
		*) die "Unsupported architecture" ;;
	esac
}

src_compile() {
	emake -j1 \
		ARCH="$(bionic_arch $ARCH)" \
		CC="$(tc-getCC)" \
		CXX="$(tc-getCXX)" \
		LD="$(tc-getLD)" \
		AR="$(tc-getAR)"
}

src_install() {
	emake install \
		ARCH="$(bionic_arch $ARCH)" \
		PREFIX="${EPREFIX}/usr" \
		DESTDIR="${D}"
}
